use serde::Deserialize;
use serde_xml_rs::from_reader;
use std::fs::File;
use std::io::*;

pub fn load_xml(file: &str) -> Option<Root> {
    if let Ok(f) = File::open(file) {
        match from_reader(BufReader::new(f)) {
            Ok(r) => Some(r),
            Err(e) => panic!("{:?}", e),
        }
    } else {
        None
    }
}

#[derive(Deserialize, Debug)]
pub struct Root {
    pub document: Document,
}

#[derive(Deserialize, Debug)]
pub struct Document {
    pub sentences: Sentences,
    #[serde(rename = "coreference")]
    pub coreferences: Coreferences,
}

#[derive(Deserialize, Debug)]
pub struct Sentences {
    pub sentence: Vec<Sentence>,
}

#[derive(Deserialize, Debug)]
pub struct Sentence {
    pub id: usize,
    pub tokens: Tokens,
    pub parse: String,
    pub dependencies: Vec<Dependencies>,
}

#[derive(Deserialize, Debug)]
pub struct Tokens {
    pub token: Vec<Token>,
}

#[derive(Deserialize, Debug)]
pub struct Token {
    pub id: usize,
    pub word: String,
    pub lemma: String,
    #[serde(rename = "CharacterOffsetBegin")]
    pub character_offset_begin: usize,
    #[serde(rename = "CharacterOffsetEnd")]
    pub character_offset_end: usize,
    #[serde(rename = "POS")]
    pub pos: String,
    #[serde(rename = "NER")]
    pub ner: String,
    #[serde(rename = "Speaker")]
    pub speaker: Option<String>,
    pub timex: Option<Timex>,
}

#[derive(Deserialize, Debug)]
pub struct Timex {
    pub tid: String,
    #[serde(rename = "type")]
    pub type_: String,
    #[serde(rename = "$value")]
    pub content: String,
}

#[derive(Deserialize, Debug)]
pub struct Dependencies {
    #[serde(rename = "type")]
    pub type_: String,
    pub dep: Vec<Dep>,
}

#[derive(Deserialize, Debug)]
pub struct Dep {
    #[serde(rename = "type")]
    pub type_: String,
    pub governor: Governor,
    pub dependent: Dependent,
}
#[derive(Deserialize, Debug)]
pub struct Governor {
    pub idx: usize,
    #[serde(rename = "$value")]
    pub content: String,
}

#[derive(Deserialize, Debug)]
pub struct Dependent {
    pub idx: usize,
    #[serde(rename = "$value")]
    pub content: String,
}

#[derive(Deserialize, Debug)]
pub struct Coreferences {
    pub coreference: Vec<Coreference>,
}

#[derive(Deserialize, Debug)]
pub struct Coreference {
    pub mention: Vec<Mention>,
}

#[derive(Deserialize, Debug)]
pub struct Mention {
    pub representative: Option<bool>,
    pub sentence: usize,
    pub start: usize,
    pub end: usize,
    pub head: usize,
    pub text: String,
}
