// 第6章: 英語テキストの処理
//
// 英語のテキスト（nlp.txt）に対して，以下の処理を実行せよ．
#![allow(dead_code)]
use dot;
use nlp100::coreml_xml::*;
use regex::Regex;
use rust_stemmers::{Algorithm, Stemmer};
use std::borrow::Cow;
use std::collections::HashMap;
use std::fs::File;
use std::io::*;
// use std::marker::PhantomData;
#[macro_use]
extern crate combine;
use combine::parser::char::{char, letter, spaces, string};
use combine::{between, choice, many1, none_of, parser, sep_by, Parser};
use combine::error::{ParseError};
use combine::stream::{Stream};


fn main() {
    // let all = p50("nlp.txt").expect("failed to load");
    // let ss = p51(&all);
    // for s in &ss {
    //     p52(s);
    //     println!();
    // }
    if let Some(root) = load_xml("nlp.txt.xml") {
        // println!("{:?}", root);
        // p53(&root);
        // p54(&root);
        // p55(&root);
        // p56(&root);
        p57(&root, 4);
        // p58(&root);
        p59(&root);
    }
    // for t in &v {
    //     println!("{:?}", t);
    // }
}

// 50. 文区切り
// (. or ; or : or ? or !) → 空白文字 → 英大文字というパターンを文の区切りと見なし，入力された文書を1行1文の形式で出力せよ．

fn p50(file: &str) -> Result<Vec<String>> {
    let mut input = BufReader::new(File::open(file)?);
    let mut sentence = Vec::new();
    let mut buffer = String::new();
    let junk = Regex::new(r"[\n,]").expect("wrong regex");
    let send = Regex::new(r"[.;:?!][[:blank:]]+([[:upper:]])").expect("wrong regex");
    input.read_to_string(&mut buffer)?;
    let mut buf = &buffer[..];
    while let Some(c) = send.captures(&buf) {
        let s = junk.replace_all(&buf[..c.get(1).unwrap().end()], " ");
        // println!("{}", &s[..55]);
        sentence.push(s.to_string());
        buf = &buf[c.get(1).unwrap().start()..];
    }
    Ok(sentence)
}

// 51. 単語の切り出し
// 空白を単語の区切りとみなし，50の出力を入力として受け取り，1行1単語の形式で出力せよ．ただし，文の終端では空行を出力せよ．
fn p51(v: &[String]) -> Vec<Vec<String>> {
    let mut output: Vec<Vec<String>> = Vec::new();
    let word_end = Regex::new(r"([^\.\t:;?! ]+)([\.\t:;?! ]+)").expect("wrong regex");
    for sen in v {
        let mut buf = &sen[..];
        let mut words: Vec<String> = Vec::new();
        while let Some(c) = word_end.captures(&buf) {
            let s = &buf[..c.get(1).unwrap().end()];
            println!("{}", s);
            words.push(s.to_string());
            buf = &buf[c.get(0).unwrap().end()..];
        }
        output.push(words);
        println!();
    }
    output
}

// 52. ステミング
// 51の出力を入力として受け取り，Porterのステミングアルゴリズムを適用し，単語と語幹をタブ区切り形式で出力せよ． Pythonでは，Porterのステミングアルゴリズムの実装としてstemmingモジュールを利用するとよい．
fn p52(s: &[String]) -> Vec<(String, String)> {
    let mut output: Vec<(String, String)> = Vec::new();
    let stemmer = Stemmer::create(Algorithm::English);
    for word in s {
        println!("{}\t{}", stemmer.stem(word), word);
        output.push((stemmer.stem(word).to_string(), word.to_string()));
    }
    output
}

// 53. Tokenization
// Stanford Core NLPを用い，入力テキストの解析結果をXML形式で得よ．また，このXMLファイルを読み込み，入力テキストを1行1単語の形式で出力せよ．
fn p53(root: &Root) {
    for sentence in root.document.sentences.sentence.iter() {
        for t in sentence.tokens.token.iter() {
            println!("{}", t.word);
        }
    }
}

// 54. 品詞タグ付け
// Stanford Core NLPの解析結果XMLを読み込み，単語，レンマ，品詞をタブ区切り形式で出力せよ．
fn p54(root: &Root) {
    for sentence in root.document.sentences.sentence.iter() {
        for t in sentence.tokens.token.iter() {
            println!("{}\t{}\t{}", t.word, t.lemma, t.pos);
        }
    }
}

// 55. 固有表現抽出
// 入力文中の人名をすべて抜き出せ．
fn p55(root: &Root) {
    for sentence in root.document.sentences.sentence.iter() {
        for t in sentence.tokens.token.iter() {
            if t.ner == "PERSON" {
                println!("{}\t{}\t{}", t.word, t.lemma, t.pos);
            }
        }
    }
}

// 56. 共参照解析
// Stanford Core NLPの共参照解析の結果に基づき，文中の参照表現（mention）を代表参照表現（representative mention）に置換せよ．ただし，置換するときは，「代表参照表現（参照表現）」のように，元の参照表現が分かるように配慮せよ．
fn p56(root: &Root) {
    let mut rep: Option<&Mention> = None;
    for c in &root.document.coreferences.coreference {
        for m in c.mention.iter() {
            if m.representative == Some(true) {
                rep = Some(m);
                continue;
            }
            for t in root.document.sentences.sentence[m.sentence-1].tokens.token.iter() {
                if t.id == m.start {
                    print!("{}(", rep.unwrap().text);
                }
                print!("{}", t.word);
                if t.id == m.end - 1 {
                    print!(")");
                }
                print!(" ");
            }
            println!();
        }
    }
}

// 57. 係り受け解析
// Stanford Core NLPの係り受け解析の結果（collapsed-dependencies）を有向グラフとして可視化せよ．可視化には，係り受け木をDOT言語に変換し，Graphvizを用いるとよい．また，Pythonから有向グラフを直接的に可視化するには，pydotを使うとよい．
type Nd = usize;
type Ed = (usize, usize);

struct S<'a> {
    node: HashMap<usize, &'a str>,
    edge: HashMap<Ed, bool>,
}

impl<'a> dot::Labeller<'a, Nd, Ed> for S<'a> {
    fn graph_id(&'a self) -> dot::Id<'a> {
        dot::Id::new("p57").unwrap()
    }
    fn node_id(&'a self, n: &Nd) -> dot::Id<'a> {
        dot::Id::new(format!("N{}", *n)).unwrap()
    }
    fn node_label(&'a self, n: &Nd) -> dot::LabelText<'a> {
        if let Some(l) = self.node.get(n) {
            dot::LabelText::HtmlStr((*l).into())
        } else {
            dot::LabelText::HtmlStr("???".into())
        }
    }
}

impl<'a> dot::GraphWalk<'a, Nd, Ed> for S<'a> {
    fn nodes(&self) -> dot::Nodes<'a, Nd> {
        Cow::Owned((0..self.node.len()).collect())
    }

    fn edges(&'a self) -> dot::Edges<'a, Ed> {
        let mut edges: Vec<Ed> = Vec::new();
        for (e, _) in self.edge.iter() {
            edges.push(*e);
        }
        Cow::Owned(edges)
    }
    fn source(&self, e: &Ed) -> Nd {
        e.0
    }
    fn target(&self, e: &Ed) -> Nd {
        e.1
    }
}

fn p57(root: &Root, n :usize) {
    let s = &root.document.sentences.sentence[n];
    let mut node: HashMap<usize, &str> = HashMap::new();
    let mut edge: HashMap<(usize, usize), bool> = HashMap::new();
    for ds in s.dependencies.iter() {
        for d in ds.dep.iter() {
            node.insert(d.governor.idx, d.governor.content.as_str());
            node.insert(d.dependent.idx, d.dependent.content.as_str());
            edge.insert((d.governor.idx, d.dependent.idx), true);
        }
    }
    let s = S { node, edge };
    let mut f = File::create("example.dot").expect("failed to create example.dot");
    // $ dot -Tpng example.dot > example.png && open example.png
    dot::render(&s, &mut f).expect("failed to dump a dot.");
}

// 58. タプルの抽出
// Stanford Core NLPの係り受け解析の結果（collapsed-dependencies）に基づき，「主語 述語 目的語」の組をタブ区切り形式で出力せよ．ただし，主語，述語，目的語の定義は以下を参考にせよ．
//
// 述語: nsubj関係とdobj関係の子（dependant）を持つ単語
// 主語: 述語からnsubj関係にある子（dependent）
// 目的語: 述語からdobj関係にある子（dependent）
fn p58(root: &Root) {
    for s in root.document.sentences.sentence.iter() {
        let mut to_sub: HashMap<_, _> = HashMap::new();
        let mut to_obj: HashMap<_, _> = HashMap::new();
        for ds in s.dependencies.iter() {
            for d in ds.dep.iter() {
                match d.type_.as_str() {
                    "nsubj" => to_sub.insert(&d.governor.content, &d.dependent.content),
                    "dobj" => to_obj.insert(&d.governor.content, &d.dependent.content),
                    _ => None,
                };
            }
            for (key, sub) in to_sub.iter() {
                if let Some(obj) = to_obj.get(key) {
                    println!("{}\t{}\t{}", sub, key, obj);
                }
            }
        }
    }
}

// 59. S式の解析
// Stanford Core NLPの句構造解析の結果（S式）を読み込み，文中のすべての名詞句（NP）を表示せよ．入れ子になっている名詞句もすべて表示すること．
enum Sexp {
    List(Vec<Sexp>),
    Atom(String),
}
/*
struct SexpParser<I>(PhantomData<fn(I) -> I>);

impl<I> Parser for SexpParser<I> where I: Stream<Item=char> {
    type Input = I;
    type Output = Sexp;
    fn parse_stream(&mut self, input: I) -> ParseResult<Self::Output, Self::Input> {
        let mut parser = many1::<String, _>(digit()).map(|s| Sexp::Atom(s));
        parser.parse_stream(input)
    }
}
*/

#[derive(Debug, PartialEq)]
pub enum Expr {
    Id(String),
    Array(Vec<Expr>),
}

// `impl Parser` can be used to create reusable parsers with zero overhead
fn expr_<I>() -> impl Parser<Input = I, Output = Expr>
    where I: Stream<Item = char>,
          // Necessary due to rust-lang/rust#24159
          I::Error: ParseError<I::Item, I::Range, I::Position>,
{
    let word = many1(none_of(" ()".chars()));
    // let word = many1(letter());

    // A parser which skips past whitespace.
    // Since we aren't interested in knowing that our expression parser
    // could have accepted additional whitespace between the tokens we also silence the error.
    let skip_spaces = || spaces().silent();

    //Creates a parser which parses a char and skips any trailing whitespace
    let lex_char = |c| char(c).skip(skip_spaces());

    let comma_list = sep_by(expr(), spaces());
    let array = between(lex_char('('), lex_char(')'), comma_list);

    //We can use tuples to run several parsers in sequence
    //The resulting type is a tuple containing each parsers output

    choice((
        word.map(Expr::Id),
        array.map(Expr::Array),
    ))
        .skip(skip_spaces())
}

parser!{
    fn expr[I]()(I) -> Expr
    where [I: Stream<Item = char>]
    {
        expr_()
    }
}

fn test() {
    let result = expr().parse("((hello world) ok)");
    let expr = Expr::Array(vec![
        Expr::Array(vec![Expr::Id("hello".to_string()),
                         Expr::Id("world".to_string())]),
        Expr::Id("ok".to_string())
            ]);
    assert_eq!(result, Ok((expr, "")));
    println!("done");
}

fn parse_down(s: &Expr) {
    match s {
        Expr::Id(_) => { }
        Expr::Array(v) => {
            if Some(&Expr::Id("NP".to_string())) == v.get(0) {
                println!("{}", flatten(s));
            }
            for i in v.iter().skip(1) {
                parse_down(i);
            }
        }
    }
}

fn flatten(s: &Expr) -> String {
    match s {
        Expr::Id(s) => s.to_string(),
        Expr::Array(v) => v.iter()
            .skip(1)
            .map(|s| flatten(s))
            .collect::<Vec<String>>()
            .join(" ").to_string(),
    }
}

fn p59(root: &Root) {
    for s in root.document.sentences.sentence.iter() {
        if let Ok(s) = expr().parse(s.parse.as_str()) {
            println!("{:?}", &s.0);
            println!("{:?}", parse_down(&s.0));
        }
    }
}

