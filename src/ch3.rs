#![allow(dead_code)]
/// 第3章: 正規表現
// Wikipediaの記事を以下のフォーマットで書き出したファイルjawiki-country.json.gzがある．
//
// 1行に1記事の情報がJSON形式で格納される
// 各行には記事名が"title"キーに，記事本文が"text"キーの辞書オブジェクトに格納され，そのオブジェクトがJSON形式で書き出される
// ファイル全体はgzipで圧縮される
// 以下の処理を行うプログラムを作成せよ．
//
use regex::Regex;
use serde::{Deserialize, Serialize};
use serde_json::{from_value, Deserializer, Value};
use std::collections::HashMap;
use std::fs::File;
use std::io::*;

#[derive(Serialize, Deserialize, Debug)]
struct Article {
    title: String,
    text: String,
}

// 20. JSONデータの読み込み
// Wikipedia記事のJSONファイルを読み込み，「イギリス」に関する記事本文を表示せよ．問題21-29では，ここで抽出した記事本文に対して実行せよ．
fn main() {
    let arts = build().expect("error");
    let mut target: Option<&Article> = None;
    for a in &arts {
        if a.title.contains("イギリス") {
            target = Some(a);
            break;
        }
    }
    if let Some(eng) = target {
        // p21(eng);
        // p22(eng);
        // p23(eng);
        // p24(eng);
        // p25(eng);
        // p26(eng);
        // p27(eng);
        // p28(eng);
        p29(eng);
    }
}

fn build() -> std::io::Result<Vec<Article>> {
    let input = BufReader::new(File::open("jawiki-country.json")?);
    let data = Deserializer::from_reader(input).into_iter::<Value>();
    let mut vec: Vec<Article> = Vec::new();
    for v in data {
        vec.push(from_value(v?)?);
    }
    Ok(vec)
}

// 21. カテゴリ名を含む行を抽出
// 記事中でカテゴリ名を宣言している行を抽出せよ．
fn p21(target: &Article) {
    let re = Regex::new(r"\[\[Category:.*\]\]").unwrap();
    for l in target.text.lines() {
        if re.is_match(l) {
            println!("{}", l);
        }
    }
}

// 22. カテゴリ名の抽出
// 記事のカテゴリ名を（行単位ではなく名前で）抽出せよ．
fn p22(target: &Article) {
    let re = Regex::new(r"\[\[Category:(.*)\]\]").unwrap();
    for l in target.text.lines() {
        if let Some(c) = re.captures(l) {
            println!("{}", &c[1]);
        }
    }
}

// 23. セクション構造
// 記事中に含まれるセクション名とそのレベル（例えば"== セクション名 =="なら1）を表示せよ．
fn p23(target: &Article) {
    let re = Regex::new(r"(=+) (.*) (=+)").unwrap();
    for l in target.text.lines() {
        if let Some(c) = re.captures(l) {
            println!("{} {}", &c[2], c[1].len().min(c[3].len()));
        }
    }
}

// 24. ファイル参照の抽出
// 記事から参照されているメディアファイルをすべて抜き出せ．
fn p24(target: &Article) {
    let re = Regex::new(r"\[\[(ファイル|File):([^|]+)\|").unwrap();
    for l in target.text.lines() {
        if let Some(c) = re.captures(l) {
            println!("{}", &c[2]);
        }
    }
}

// 25. テンプレートの抽出
// 記事中に含まれる「基礎情報」テンプレートのフィールド名と値を抽出し，辞書オブジェクトとして格納せよ．
fn p25(target: &Article) {
    let mut hash: HashMap<String, String> = HashMap::new();
    let start = Regex::new(r"^\{\{基礎情報").unwrap();
    let end = Regex::new(r"^\}\}$").unwrap();
    let template = Regex::new(r"^\|([^=]+) = (.*)$").unwrap();
    let mut iter = target.text.lines();
    'end: while let Some(l) = iter.next() {
        if let Some(_) = start.captures(l) {
            while let Some(l) = iter.next() {
                if end.is_match(l) {
                    break 'end;
                } else if let Some(c) = template.captures(l) {
                    hash.insert(c[1].to_string(), c[2].to_string());
                    // println!("'{}' : '{}'", &c[1], &c[2]);
                }
            }
        }
    }
    println!("{:?}", hash);
}

// 26. 強調マークアップの除去
// 25の処理時に，テンプレートの値からMediaWikiの強調マークアップ（弱い強調，強調，強い強調のすべて）を除去してテキストに変換せよ（参考: マークアップ早見表）．
fn p26(target: &Article) {
    let mut hash: HashMap<String, String> = HashMap::new();
    let start = Regex::new(r"^\{\{基礎情報").unwrap();
    let end = Regex::new(r"^\}\}$").unwrap();
    let template = Regex::new(r"^\|([^=]+) = (.*)$").unwrap();
    let quotes = Regex::new(r"'+").unwrap();
    let mut iter = target.text.lines();
    'end: while let Some(l) = iter.next() {
        if let Some(_) = start.captures(l) {
            while let Some(l) = iter.next() {
                if end.is_match(l) {
                    break 'end;
                } else if let Some(c) = template.captures(l) {
                    hash.insert(c[1].to_string(), quotes.replace_all(&c[2], "").to_string());
                    // println!("'{}' : '{}'", &c[1], &c[2]);
                }
            }
        }
    }
    println!("{:?}", hash);
}

// 27. 内部リンクの除去
// 26の処理に加えて，テンプレートの値からMediaWikiの内部リンクマークアップを除去し，テキストに変換せよ（参考: マークアップ早見表）．
fn p27(target: &Article) {
    let mut hash: HashMap<String, String> = HashMap::new();
    let start = Regex::new(r"^\{\{基礎情報").unwrap();
    let end = Regex::new(r"^\}\}$").unwrap();
    let template = Regex::new(r"^\|([^=]+) = (.*)$").unwrap();
    let quotes = Regex::new(r"'+").unwrap();
    let link1 = Regex::new(r"\[\[[^#|]]+#[^|]+\|(?P<display>[^]]+)\]\]").expect("wrong link1");
    let link2 = Regex::new(r"\[\[[^]|]+\|(?P<display>[^]]+)\]\]").expect("wrong link2");
    let link3 = Regex::new(r"\[\[(?P<display>[^]]*)\]\]").expect("wrong link3");
    let mut iter = target.text.lines();
    'end: while let Some(l) = iter.next() {
        if let Some(_) = start.captures(l) {
            while let Some(l) = iter.next() {
                if end.is_match(l) {
                    break 'end;
                } else if let Some(c) = template.captures(l) {
                    let no_q = quotes.replace_all(&c[2], "");
                    let no_l1 = link1.replace_all(&no_q, "$display");
                    let no_l2 = link2.replace_all(&no_l1, "$display");
                    let no_l3 = link3.replace_all(&no_l2, "$display");
                    hash.insert(c[1].to_string(), no_l3.to_string());
                }
            }
        }
    }
    println!("{:?}", hash);
}

// 28. MediaWikiマークアップの除去
// 27の処理に加えて，テンプレートの値からMediaWikiマークアップを可能な限り除去し，国の基本情報を整形せよ．
fn p28(target: &Article) {
    let mut hash: HashMap<String, String> = HashMap::new();
    let start = Regex::new(r"^\{\{基礎情報").unwrap();
    let end = Regex::new(r"^\}\}$").unwrap();
    let template = Regex::new(r"^\|([^=]+) = (.*)$").unwrap();
    let quotes = Regex::new(r"'+").unwrap();
    let link1 = Regex::new(r"\[\[[^#|]]+#[^|]+\|(?P<display>[^]]+)\]\]").expect("wrong link1");
    let link2 = Regex::new(r"\[\[[^]|]+\|(?P<display>[^]]+)\]\]").expect("wrong link2");
    let link3 = Regex::new(r"\[\[(?P<display>[^]]*)\]\]").expect("wrong link3");
    let lang = Regex::new(r"\{\{lang\|..\|(?P<display>[^}]*)\}\}").expect("wrong lang tag");
    let inner = Regex::new(r"\{\{(?P<display>[^}]*)\}\}").expect("wrong inner link");
    let tag_ref1 = Regex::new(r"<ref>[^<]+</ref>").expect("wrong rel1");
    let tag_ref2 = Regex::new(r"<ref[^/]*/>").expect("wrong rel");
    let tag_br = Regex::new(r"<br[^/]*/>").expect("wrong brl");
    let mut iter = target.text.lines();
    'end: while let Some(l) = iter.next() {
        if let Some(_) = start.captures(l) {
            while let Some(l) = iter.next() {
                if end.is_match(l) {
                    break 'end;
                } else if let Some(c) = template.captures(l) {
                    let mut trimmed = quotes.replace_all(&c[2], "").to_string();
                    for re in &[&link1, &link2, &link3, &lang, &inner] {
                        trimmed = re.replace_all(&trimmed, "$display").to_string();
                    }
                    for re in &[&tag_ref1, &tag_ref2, &tag_br] {
                        trimmed = re.replace_all(&trimmed, "").to_string();
                    }
                    hash.insert(c[1].to_string(), trimmed);
                }
            }
        }
    }
    for (k, d) in &hash {
        println!("{:<10}\t| {}", k, d);
    }
}

// 29. 国旗画像のURLを取得する
// テンプレートの内容を利用し，国旗画像のURLを取得せよ．（ヒント: MediaWiki APIのimageinfoを呼び出して，ファイル参照をURLに変換すればよい）
fn p29(target: &Article) {
    let mut hash: HashMap<String, String> = HashMap::new();
    let start = Regex::new(r"^\{\{基礎情報").unwrap();
    let end = Regex::new(r"^\}\}$").unwrap();
    let template = Regex::new(r"^\|([^=]+) = (.*)$").unwrap();
    let quotes = Regex::new(r"'+").unwrap();
    let tag_ref1 = Regex::new(r"<ref>[^<]+</ref>").expect("wrong rel1");
    let tag_ref2 = Regex::new(r"<ref[^/]*/>").expect("wrong rel");
    let tag_br = Regex::new(r"<br[^/]*/>").expect("wrong brl");
    let mut iter = target.text.lines();
    'end: while let Some(l) = iter.next() {
        if let Some(_) = start.captures(l) {
            while let Some(l) = iter.next() {
                if end.is_match(l) {
                    break 'end;
                } else if let Some(c) = template.captures(l) {
                    let mut trimmed = quotes.replace_all(&c[2], "").to_string();
                    for re in &[&tag_ref1, &tag_ref2, &tag_br] {
                        trimmed = re.replace_all(&trimmed, "").to_string();
                    }
                    hash.insert(c[1].to_string(), trimmed);
                }
            }
        }
    }
    if let Some(p) = hash.get("国旗画像") {
        let space = Regex::new(r" ").expect("wrong space");
        let title = space.replace_all(p, "%20").to_string();
        println!(
            "https://www.mediawiki.org/w/api.php?action=query&titles=File:{}&prop=imageinfo",
            title,
        );
    }
}
