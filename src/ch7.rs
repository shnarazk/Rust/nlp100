#![allow(dead_code)]
// #[macro_use(bson, doc)]
use actix_web::{http, server, App, HttpRequest, HttpResponse, Query};
// mongod --unixSocketPrefix=./mongo --dbpath=./mongo --port=10060 --logpath=./mongo/mongod.log --pidfilepath=./mongo/mongod.pid
//
// # For MongoDB Atlas
// const MongoClient = require('mongodb').MongoClient;
// const uri = "mongodb+srv://admin:<password>@cluster0-6krqd.mongodb.net/test?retryWrites=true&w=majority";
// const client = new MongoClient(uri, { useNewUrlParser: true });
// client.connect(err => {
//   const collection = client.db("test").collection("devices");
//   // perform actions on the collection object
//   client.close();
// });
//
use mongodb::{bson, coll::options::IndexModel, db::ThreadedDatabase, doc, Bson, ThreadedClient};
// use regex::Regex;
use serde::{Deserialize, Serialize};
use serde_json::{from_value, Deserializer, Value};
/// ### usage of Redis
/// - start: `redis-server`
/// - shutdown: `redis-cli shutdown`
// use simple_redis::{client::Client, create};
// use std::collections::HashMap;
use std::fs::File;
use std::io::stdout;
use std::io::*;
use std::ops::Neg;

fn main() {
    // let mut r = simple_redis::create("redis://127.0.0.1:6379/").expect("Unable to create Redis client");
    // let json = load().expect("no good");
    // p60(&mut r, &json);
    // p61(&mut r);
    // p62(&mut r);
    // p63(&mut r, &json);
    // let m = mongodb::Client::connect("localhost", 10060).expect("Failed to connect");
    // mongo_test();
    // p64(&mut m, &json);  // build
    // p65(&mut m);
    // p66(&mut m);
    // p67(&mut m);
    // p68(&mut m);
    p69();
}

fn mongo_test() {
    let client = mongodb::Client::connect("localhost", 10060).expect("Failed to connect");
    let coll = client.db("test").collection("artists");
    let s = vec![Bson::from(""), Bson::from("a")];
    let doc = doc! {
        "name": "Dead Man",
        "aliases": s,
        "tags": 0,
        "rating" : 0,
    };
    match coll.insert_one(doc.clone(), None) {
        Ok(o) => println!("inserted {:?}", o),
        Err(e) => panic!("inserted {:?}", e),
    }
    let mut cursor = coll
        .find(Some(doc.clone()), None)
        .ok()
        .expect("Failed to execute find.");
    let item = cursor.next();
    match item {
        Some(Ok(d)) => match (d.get("name"), d.get("rating")) {
            (Some(&Bson::String(ref n)), Some(&Bson::I32(ref r))) => println!("{} {:?}", n, r),
            _ => println!("Expected title to be a string!"),
        },
        Some(Err(_)) => println!("Failed to get next from server!"),
        None => println!("Server returned no results!"),
    }
    client.db("test").drop_database().expect("failed to close");
}

fn json_test() {
    let v = load().expect("no good");
    for a in &v {
        if let Some(p) = &a.area {
            if p == "Japan" {
                println!("{} at {}", &a.name, p);
            }
        }
    }
}

fn redis_test() {
    let mut client =
        simple_redis::create("redis://127.0.0.1:6379/").expect("Unable to create Redis client");
    match client.set("my_key", "my_value") {
        Err(error) => println!("Unable to set value in Redis: {}", error),
        _ => println!("Value set in Redis"),
    }
    match client.get_string("my_key") {
        Ok(value) => println!("Read value from Redis: {}", value),
        Err(error) => println!("Unable to get value from Redis: {}", error),
    }
}

// 第7章: データベース
//
// artist.json.gzは，オープンな音楽データベースMusicBrainzの中で，アーティストに関するものをJSON形式に変換し，gzip形式で圧縮したファイルである．このファイルには，1アーティストに関する情報が1行にJSON形式で格納されている．JSON形式の概要は以下の通りである．
//
// フィールド	型	内容	例
// id	ユニーク識別子	整数	20660
// gid	グローバル識別子	文字列	"ecf9f3a3-35e9-4c58-acaa-e707fba45060"
// name	アーティスト名	文字列	"Oasis"
// sort_name	アーティスト名（辞書順整列用）	文字列	"Oasis"
// area	活動場所	文字列	"United Kingdom"
// aliases	別名	辞書オブジェクトのリスト
// aliases[].name	別名	文字列	"オアシス"
// aliases[].sort_name	別名（整列用）	文字列	"オアシス"
// begin	活動開始日	辞書
// begin.year	活動開始年	整数	1991
// begin.month	活動開始月	整数
// begin.date	活動開始日	整数
// end	活動終了日	辞書
// end.year	活動終了年	整数	2009
// end.month	活動終了月	整数	8
// end.date	活動終了日	整数	28
// tags	タグ	辞書オブジェクトのリスト
// tags[].count	タグ付けされた回数	整数	1
// tags[].value	タグ内容	文字列	"rock"
// rating	レーティング	辞書オブジェクト
// rating.count	レーティングの投票数	整数	13
// rating.value	レーティングの値（平均値）	整数	86
// artist.json.gzのデータをKey-Value-Store (KVS) およびドキュメント志向型データベースに格納・検索することを考える．KVSとしては，LevelDB，Redis，KyotoCabinet等を用いよ．ドキュメント志向型データベースとして，MongoDBを採用したが，CouchDBやRethinkDB等を用いてもよい．

#[derive(Serialize, Deserialize, Debug)]
struct Name {
    name: String,      // 別名 "オアシス"
    sort_name: String, // 別名（整列用） "オアシス"
}

#[derive(Serialize, Deserialize, Debug)]
struct Date {
    year: Option<isize>,  // 活動開始年 1991
    month: Option<isize>, // 活動開始月
    date: Option<isize>,  // 活動開始日
}

#[derive(Serialize, Deserialize, Debug)]
struct Tag {
    count: isize,  // タグ付けされた回数 1
    value: String, // タグ内容 "rock"
}

#[derive(Serialize, Deserialize, Debug)]
struct Rating {
    count: isize, // レーティングの投票数 13
    value: isize, // レーティングの値（平均値） 86
}

#[derive(Serialize, Deserialize, Debug)]
struct Artist {
    id: isize,                  // ユニーク識別子 20660
    gid: String,                // グローバル識別子 "ecf9f3a3-35e9-4c58-acaa-e707fba45060"
    name: String,               // アーティスト名 "Oasis"
    sort_name: String,          // アーティスト名（辞書順整列用） "Oasis"
    area: Option<String>,       // 活動場所 "United Kingdom"
    aliases: Option<Vec<Name>>, // 別名	辞書オブジェクトのリスト
    begin: Option<Date>,        // 活動開始日 辞書オブジェクト
    end: Option<Date>,          // 活動終了日 辞書オブジェクト
    tags: Option<Vec<Tag>>,     // タグ	辞書オブジェクトのリスト
    rating: Option<Rating>,     // レーティング 辞書オブジェクト
}

fn load() -> std::io::Result<Vec<Artist>> {
    let input = BufReader::new(File::open("artist.json")?);
    let data = Deserializer::from_reader(input).into_iter::<Value>();
    let mut vec: Vec<Artist> = Vec::new();
    for v in data {
        vec.push(from_value(v?)?);
    }
    Ok(vec)
}

// 60. KVSの構築
// Key-Value-Store (KVS) を用い，アーティスト名（name）から活動場所（area）を検索するためのデータベースを構築せよ．
fn p60(server: &mut simple_redis::client::Client, db: &Vec<Artist>) {
    for a in db {
        let area: &str = match &a.area {
            Some(p) => &p,
            None => &"",
        };
        if server.set(a.name.as_str(), area).is_err() {
            println!("Unable to set value in Redis");
        }
    }
}

// 61. KVSの検索
// 60で構築したデータベースを用い，特定の（指定された）アーティストの活動場所を取得せよ．
fn p61(r: &mut simple_redis::client::Client) {
    let mut buf = String::new();
    loop {
        print!("Artist: ");
        stdout().flush().unwrap();
        match stdin().read_line(&mut buf) {
            Ok(0) => break,
            Ok(_) => {
                let key: &str = &buf[..buf.len() - 1];
                match r.get_string(key) {
                    Ok(value) => println!(" playing in {}", value),
                    Err(_) => println!("Unable to get value from Redis"),
                }
                buf.clear();
            }
            _ => break,
        }
    }
}

// 62. KVS内の反復処理
// 60で構築したデータベースを用い，活動場所が「Japan」となっているアーティスト数を求めよ．
fn p62(r: &mut simple_redis::client::Client) {
    let mut cnt = 0;
    for a in r.keys("*").expect("fail to connect db") {
        match r.get_string(&a) {
            Ok(ref p) if p == "Japan" => {
                cnt += 1;
                if cnt % 1000 == 0 {
                    println!("{:>6}: {}", cnt, &a);
                }
            }
            _ => (),
        }
    }
    println!("total {}.", cnt);
}

// 63. オブジェクトを値に格納したKVS
// KVSを用い，アーティスト名（name）からタグと被タグ数（タグ付けされた回数）のリストを検索するためのデータベースを構築せよ．さらに，ここで構築したデータベースを用い，アーティスト名からタグと被タグ数を検索せよ．
fn p63(r: &mut simple_redis::client::Client, db: &Vec<Artist>) {
    for a in db {
        if let Some(tags) = &a.tags {
            for tag in tags {
                // println!("{} {} {}", a.name.as_str(), &tag.value, tag.count);
                if r.hset(&format!("hash:{}", a.name), &tag.value, tag.count)
                    .is_err()
                {
                    println!("Unable to set value in Redis");
                }
            }
        }
    }
    println!("{:?}", r.hgetall("hash:Perfume"));
}

// 64. MongoDBの構築
// アーティスト情報（artist.json.gz）をデータベースに登録せよ．さらに，次のフィールドでインデックスを作成せよ: name, aliases.name, tags.value, rating.value
fn p64(client: &mut mongodb::Client, db: &Vec<Artist>) {
    println!("start building.");
    let coll = client.db("test").collection("artists");
    let mut count: usize = 0;
    for a in db {
        let aliases = match &a.aliases {
            Some(v) => v.iter().map(|n| Bson::from(&n.name)).collect(),
            None => vec![Bson::from(vec![])],
        };
        let tags = match &a.tags {
            Some(v) => v.iter().map(|t| Bson::from(&t.value)).collect(),
            None => vec![Bson::from(vec![])],
        };
        let area = match &a.area {
            Some(p) => p.to_string(),
            None => "".to_string(),
        };
        let doc = doc! {
            "name": &a.name,
            "aliases": aliases,
            "area": area,
            "tags": tags,
            "rating.count": match &a.rating {
                Some(r) => r.count as i64,
                None => 0i64,
            },
            "rating.value": match &a.rating {
                Some(r) => r.value as i64,
                None => 0i64,
            },
        };
        match coll.insert_one(doc.clone(), None) {
            Ok(_) => {
                count += 1;
                print!("\x1B[1G\x1B[0K\x1B[032mInsert {}...\x1B[000m", count);
                stdout().flush().unwrap();
            }
            Err(e) => panic!("inserted {:?}", e),
        }
    }
    println!("creating indexes");
    coll.create_indexes(vec![
        IndexModel::new(doc! {"name": 1 }, None),
        IndexModel::new(doc! {"aliases": 1 }, None),
        IndexModel::new(doc! {"tags": 1 }, None),
        IndexModel::new(doc! {"rating.value": 1 }, None),
    ])
    .expect("fail to create indexes.");
    println!("done.");
}

//
// 65. MongoDBの検索
// MongoDBのインタラクティブシェルを用いて，"Queen"というアーティストに関する情報を取得せよ．さらに，これと同様の処理を行うプログラムを実装せよ．
fn p65(client: &mut mongodb::Client) {
    let coll = client.db("test").collection("artists");
    let mut buf = String::new();
    loop {
        print!("Artist: ");
        stdout().flush().unwrap();
        match stdin().read_line(&mut buf) {
            Ok(0) => break,
            Ok(_) => {
                if buf == "\n" {
                    break;
                }
                let key: &str = &buf[..buf.len() - 1];
                match coll.find(Some(doc! { "name": key }), None) {
                    Ok(cursor) => {
                        for result in cursor {
                            if let Ok(item) = result {
                                if let Some(&Bson::Array(ref a)) = item.get("aliases") {
                                    print!(" aliases:");
                                    for i in a {
                                        if let Some(s) = i.as_str() {
                                            print!(" {} ", s);
                                        }
                                    }
                                    println!();
                                }
                                if let Some(&Bson::Array(ref t)) = item.get("tags") {
                                    print!(" tags   :");
                                    for i in t {
                                        if let Some(s) = i.as_str() {
                                            print!(" {} ", s);
                                        }
                                    }
                                    println!();
                                }
                                if let Some(&Bson::Array(ref r)) = item.get("ratings.value") {
                                    print!(" ratings:");
                                    for i in r {
                                        if let Some(s) = i.as_str() {
                                            print!(" {} ", s);
                                        }
                                    }
                                    println!();
                                }
                            }
                        }
                    }
                    Err(_) => println!("None about {}", key),
                }
                buf.clear();
            }
            _ => break,
        }
    }
}

// 66. 検索件数の取得
// MongoDBのインタラクティブシェルを用いて，活動場所が「Japan」となっているアーティスト数を求めよ．
fn p66(client: &mut mongodb::Client) {
    let coll = client.db("test").collection("artists");
    let mut buf = String::new();
    loop {
        print!("Place: ");
        stdout().flush().unwrap();
        match stdin().read_line(&mut buf) {
            Ok(0) => break,
            Ok(_) => {
                if buf == "\n" {
                    break;
                }
                let key: &str = &buf[..buf.len() - 1];
                match coll.count(Some(doc! { "area": key.to_string() }), None) {
                    Ok(n) => {
                        println!("{} artists perform there.", n);
                    }
                    Err(_) => (),
                }
                buf.clear();
            }
            Err(_) => (),
        }
    }
}

// 67. 複数のドキュメントの取得
// 特定の（指定した）別名を持つアーティストを検索せよ．
//
fn p67(client: &mut mongodb::Client) {
    let coll = client.db("test").collection("artists");
    let mut buf = String::new();
    loop {
        print!("Alias: ");
        stdout().flush().unwrap();
        match stdin().read_line(&mut buf) {
            Ok(0) => break,
            Ok(_) => {
                if buf == "\n" {
                    break;
                } else if buf == "?\n" {
                    println!("Show all artists with aliases.");
                    match coll.find(None, None) {
                        Ok(cursor) => {
                            for result in cursor {
                                if let Ok(item) = result {
                                    if let Some(&Bson::Array(ref v)) = item.get("aliases") {
                                        if v.iter().any(|s| !s.as_str().unwrap_or("").is_empty()) {
                                            println!(
                                                "{} has {}.",
                                                item.get("name").unwrap(),
                                                item.get("aliases").unwrap()
                                            );
                                        }
                                    }
                                }
                            }
                        }
                        Err(_) => println!("No data found"),
                    }
                    break;
                }
                let key: &str = &buf[..buf.len() - 1];
                match coll.find(Some(doc! { "aliases": key.to_string() }), None) {
                    Ok(cursor) => {
                        for result in cursor {
                            if let Ok(item) = result {
                                if let Some(&Bson::Array(ref v)) = item.get("aliases") {
                                    if v.iter().any(|s| s.as_str().unwrap_or("") == key) {
                                        println!("{} has {:?}.", item.get("name").unwrap(), v);
                                    }
                                }
                            }
                        }
                    }
                    Err(_) => println!("None about {}", key),
                }
                buf.clear();
            }
            _ => break,
        }
    }
}

// 68. ソート
// "dance"というタグを付与されたアーティストの中でレーティングの投票数が多いアーティスト・トップ10を求めよ．
//
fn p68(client: &mut mongodb::Client) {
    let coll = client.db("test").collection("artists");
    let mut buf = String::new();
    loop {
        print!("tag: ");
        stdout().flush().unwrap();
        match stdin().read_line(&mut buf) {
            Ok(0) => break,
            Ok(_) => {
                if buf == "\n" {
                    break;
                } else if buf == "?\n" {
                    println!("Show 100 artists with tags.");
                    match coll.find(None, None) {
                        Ok(cursor) => {
                            let mut count = 0;
                            for result in cursor {
                                if let Ok(item) = result {
                                    if let Some(&Bson::Array(ref v)) = item.get("tags") {
                                        if v.iter().any(|s| !s.as_str().unwrap_or("").is_empty()) {
                                            println!(
                                                "{} has {}.",
                                                item.get("name").unwrap(),
                                                item.get("tags").unwrap()
                                            );
                                            count += 1;
                                            if 100 < count {
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        Err(_) => println!("No data found"),
                    }
                    buf.clear();
                    continue;
                }
                let key: &str = &buf[..buf.len() - 1];
                match coll.find(Some(doc! { "tags": key.to_string() }), None) {
                    Ok(cursor) => {
                        let mut vec: Vec<(i64, String)> = Vec::new();
                        for result in cursor {
                            if let Ok(item) = result {
                                // println!("{} has {}.", item.get("name").unwrap(), item.get("rating.count").unwrap());
                                match (item.get("rating.count"), item.get("name")) {
                                    (Some(c), Some(n)) => {
                                        vec.push((c.as_i64().unwrap_or(0), n.to_string()))
                                    }
                                    _ => (),
                                }
                            }
                        }
                        vec.sort_unstable();
                        for (i, (c, n)) in vec.iter().rev().enumerate().take(20) {
                            println!("{:>3}\t{:>4}\t{} ", i + 1, c, n);
                        }
                    }
                    Err(_) => println!("None about {}", key),
                }
                buf.clear();
            }
            _ => break,
        }
    }
}

// 69. Webアプリケーションの作成
// ユーザから入力された検索条件に合致するアーティストの情報を表示するWebアプリケーションを作成せよ．アーティスト名，アーティストの別名，タグ等で検索条件を指定し，アーティスト情報のリストをレーティングの高い順などで整列して表示せよ．
#[derive(Debug, Deserialize)]
struct DbQuery {
    name: String,
    alias: String,
    tag: String,
}

impl Default for DbQuery {
    fn default() -> DbQuery {
        DbQuery {
            name: "".to_string(),
            alias: "".to_string(),
            tag: "".to_string(),
        }
    }
}

fn p69() {
    server::new(|| {
        vec![
            App::new()
                .prefix("/search")
                .resource("", |r| r.method(http::Method::GET).with(|x| query(x))),
            App::new()
                .prefix("/site.css")
                .resource("", |r| r.f(send_css)),
            App::new().resource("/", |r| r.f(index)),
        ]
    })
    .bind("127.0.0.1:8088")
    .unwrap()
    .run();
}

fn query(info: Query<DbQuery>) -> HttpResponse {
    let options = mongodb::ClientOptions::with_unauthenticated_ssl(None, false);
    let m = mongodb::Client::with_uri_and_options(URI, options).expect("connect");
    let mc = m.db("admin");
    mc.auth("admin", "wino-294-mOn.").expect("auth");
    HttpResponse::Ok()
        .content_type("text/html")
        .body(
            format!(r#"<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>Artist Index</title>
<link rel="stylesheet" href="site.css">
</head>
<body>
Hello world.🦀
<form method="GET" action="/search">
<span class="qbox"><span class="qlabel">Name</span><input type="text" name="name" size="20" value="{}"></span><br/>
<span class="qbox"><span class="qlabel">Alias</span><input type="text" name="alias" size="20" value="{}"></span><br/>
<span class="qbox"><span class="qlabel">Tag</span><input type="text" name="tag" size="20" value="{}"></span><br/>
<button>SEARCH</button>
</form>
<ol>
{}
</ol>
</body>
</html>"#,
                    info.name,
                    info.alias,
                    info.tag,
                    make_response(&m, &info),
            ))
        .into()
}

fn make_response(client: &mongodb::Client, q: &DbQuery) -> String {
    let coll = client.db("test").collection("artists");
    let target = if !q.name.is_empty() {
        Some(doc! { "name": q.name.to_string() })
    } else if !q.alias.is_empty() {
        Some(doc! { "alias": q.alias.to_string() })
    } else if !q.tag.is_empty() {
        Some(doc! { "tags": q.tag.to_string() })
    } else {
        None
    };
    match coll.find(target, None) {
        Ok(cursor) => {
            let mut vec: Vec<(i64, bson::ordered::OrderedDocument)> = Vec::new();
            for result in cursor {
                if let Ok(item) = result {
                    if let Some(c) = item.get("rating.count") {
                        vec.push((c.as_i64().unwrap_or(0), item));
                    }
                }
            }
            vec.sort_by_key(|c| (c.0 as isize).neg());
            vec.iter()
                .take(10)
                .map(|(_, a)| {
                    format!(r#"<li><a href="/search?name={}&alias=&tag=">{}</a> <span class="area">{}</span><p class="tags">tags: {}</p></li>"#,
                            a.get("name").unwrap_or(&Bson::from("?")).as_str().unwrap(),
                            a.get("name").unwrap_or(&Bson::from("?")).as_str().unwrap(),
                            if let Some(at) = a.get("area") {
                                if let Some(place) = at.as_str() {
                                    if !place.is_empty() {
                                        format!(", in {}", place)
                                    } else {
                                        "".to_string()
                                    }
                                } else {
                                    "".to_string()
                                }
                            } else {
                                "".to_string()
                            },
                            if let Some(t) = a.get("tags") {
                                if let Some(v) = t.as_array() {
                                    v.iter()
                                        .map(|x|
                                             format!(r#"<a href="/search?name=&alias=&tag={}">{}</a>"#,
                                                     x.as_str().unwrap_or(""),
                                                     x.as_str().unwrap_or(""),
                                             ))
                                        .collect::<Vec<String>>().join(", ")
                                } else {
                                    "".to_string()
                                }
                            } else {
                                "".to_string()
                            }
                    )
                        .to_string()
                })
                .collect::<Vec<String>>()
                .join("")
        }
        Err(_) => "".to_string(),
    }
}

fn index(_req: &HttpRequest) -> HttpResponse {
    HttpResponse::Ok()
        .content_type("text/html")
        .body(
            r#"<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8"/>
<title>Artist Index</title>
<link rel="stylesheet" href="site.css">
</head>
<body>
Hello world.🦀 This is index page.
<form method="GET" action="/search">
<span class="qbox">Name</span><input type="text" name="name" size="20" value=""><br/>
<span class="qbox">Alias</span><input type="text" name="alias" size="20" value=""><br/>
<span class="qbox">Tag</span><input type="text" name="tag" size="20" value=""><br/>
<button>SEARCH</button>
</form>
</body>
</html>"#,
        )
        .into()
}

fn send_css(_req: &HttpRequest) -> HttpResponse {
    let buf = include_str!("../site.css");
    HttpResponse::Ok().content_type("text/css").body(buf).into()
}

/// dump the chopped json to import MongoDB Atlas
/// use:
/// ```
/// mongoimport ... --type json --jsonArray --file this.json
/// ```
fn dump(db: &Vec<Artist>) {
    let mut v = Vec::new();
    for a in db {
        let aliases = match &a.aliases {
            Some(v) => v.iter().map(|n| n.name.to_string()).collect(),
            None => vec![],
        };
        let tags = match &a.tags {
            Some(v) => v.iter().map(|t| t.value.to_string()).collect(),
            None => vec![],
        };
        let area = match &a.area {
            Some(p) => p.to_string(),
            None => "".to_string(),
        };
        let doc = json!({
            "name": a.name.to_string(),
            "aliases": aliases,
            "area": area,
            "tags": tags,
            "rating.count": match &a.rating {
                Some(r) => r.count as i64,
                None => 0i64,
            },
            "rating.value": match &a.rating {
                Some(r) => r.value as i64,
                None => 0i64,
            },
        });
        v.push(doc);
    }
    println!("{}", serde_json::to_string(&v).unwrap());
}
