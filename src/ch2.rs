#![allow(dead_code)]
use std::cmp::Ordering;
use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::io::*;

/// # 第2章: UNIXコマンドの基礎
///
/// hightemp.txtは，日本の最高気温の記録を「都道府県」「地点」「℃」「日」のタブ区切り形式で格納したファイルである．以下の処理を行うプログラムを作成し，hightemp.txtを入力ファイルとして実行せよ．さらに，同様の処理をUNIXコマンドでも実行し，プログラムの実行結果を確認せよ．
fn main() {
    // p101();
    // p11();
    // p12().expect("fail");
    // p13("col1.txt", "col2.txt", "out.txt").expect("fail");
    // let args: Vec<String> = env::args().collect();
    // let n = if args.len() <= 1 {
    //     20
    // } else {
    //     args[1].parse().unwrap_or(20)
    // };
    // p14(n);
    // p15(n);
    // let f = if 3 <= args.len() {
    //     &args[2]
    // } else {
    //     panic!("not enough args");
    // };
    // p16(n, f).unwrap();
    // p17().unwrap();
    // p18();
    p19();
    return;
}

/// 19. 各行の1コラム目の文字列の出現頻度を求め，出現頻度の高い順に並べる
/// 各行の1列目の文字列の出現頻度を求め，その高い順に並べて表示せよ．確認にはcut, uniq, sortコマンドを用いよ
fn p19() {
    let mut buf = String::new();
    let mut hash: HashMap<String, isize> = HashMap::new();
    while let Ok(n) = stdin().read_line(&mut buf) {
        if n == 0 {
            break;
        }
        let cols = buf.split_terminator('\t').collect::<Vec<&str>>();
        if let Some(n) = hash.get(&cols[0].to_string()) {
            hash.insert(cols[0].to_string(), n - 1);
        } else {
            hash.insert(cols[0].to_string(), -1);
        }
        buf.clear();
    }
    println!("{:?}", hash);
    let mut vec = hash.iter().collect::<Vec<_>>();
    vec.sort_unstable_by_key(|a| a.1);
    for (s, _) in vec {
        println!("{}", s);
    }
}

/// 18. 各行を3コラム目の数値の降順にソート
/// 各行を3コラム目の数値の逆順で整列せよ（注意: 各行の内容は変更せずに並び替えよ）．確認にはsortコマンドを用いよ（この問題はコマンドで実行した時の結果と合わなくてもよい）．
fn p18() {
    let mut buf = String::new();
    let mut vec: Vec<(f64, String)> = Vec::new();
    while let Ok(n) = stdin().read_line(&mut buf) {
        if n == 0 {
            break;
        }
        let cols = buf.split_terminator('\t').collect::<Vec<&str>>();
        vec.push((cols[2].parse::<f64>().unwrap_or(0.0), buf.to_string()));
        buf.clear();
    }
    vec.sort_unstable_by(|a, b| {
        if a.0 < b.0 {
            Ordering::Less
        } else {
            Ordering::Greater
        }
    });
    for (_, s) in vec {
        print!("{}", s);
    }
}

/// 17. １列目の文字列の異なり
/// 1列目の文字列の種類（異なる文字列の集合）を求めよ．確認にはsort, uniqコマンドを用いよ．
fn p17() -> Result<()> {
    let mut buf = String::new();
    let mut hash: HashMap<String, usize> = HashMap::new();
    while let Ok(n) = stdin().read_line(&mut buf) {
        if n == 0 {
            break;
        }
        let mut iter = buf.split_terminator('\t');
        if let Some(k) = iter.next() {
            hash.insert(k.to_string(), 0);
        }
        buf.clear();
    }
    let mut keys = hash.keys().collect::<Vec<&String>>();
    keys.sort_unstable();
    println!("{:?}", keys);
    Ok(())
}

/// 16. ファイルをN分割する
/// 自然数Nをコマンドライン引数などの手段で受け取り，入力のファイルを行単位でN分割せよ．同様の処理をsplitコマンドで実現せよ．
fn p16(n: usize, fin: &String) -> Result<()> {
    let mut input = BufReader::new(File::open(fin)?);
    let mut vec = Vec::new();
    let mut buf = String::new();
    while let Ok(k) = input.read_line(&mut buf) {
        if k == 0 {
            break;
        }
        vec.push(buf.clone());
        buf.clear();
    }
    //
    let lim = vec.len() / n;
    let mut out = format!("{}-0", fin);
    let mut fout = BufWriter::new(File::create(out)?);
    for (i, s) in vec.iter().enumerate() {
        write!(fout, "{}", s)?;
        if i % lim == lim - 1 && i + 1 < vec.len() {
            out = format!("{}-{}", fin, (i + 1) / lim);
            fout = BufWriter::new(File::create(out)?);
        }
    }
    Ok(())
}

/// 15. 末尾のN行を出力
/// 自然数Nをコマンドライン引数などの手段で受け取り，入力のうち末尾のN行だけを表示せよ．確認にはtailコマンドを用いよ．
fn p15(n: usize) {
    let mut buf = String::new();
    let mut vec = Vec::new();
    while let Ok(k) = stdin().read_line(&mut buf) {
        if k == 0 {
            break;
        }
        vec.push(buf.clone());
        buf.clear();
    }
    for i in (vec.len() - n)..vec.len() {
        print!("{}", vec[i]);
    }
}

/// 14. 先頭からN行を出力
/// 自然数Nをコマンドライン引数などの手段で受け取り，入力のうち先頭のN行だけを表示せよ．確認にはheadコマンドを用いよ．
fn p14(mut n: usize) {
    let mut buf = String::new();
    while let Ok(k) = stdin().read_line(&mut buf) {
        if n == 0 || k == 0 {
            break;
        }
        print!("{}", buf);
        n -= 1;
        buf.clear();
    }
}

/// 13. col1.txtとcol2.txtをマージ
/// 12で作ったcol1.txtとcol2.txtを結合し，元のファイルの1列目と2列目をタブ区切りで並べたテキストファイルを作成せよ．確認にはpasteコマンドを用いよ．
fn p13(f1: &str, f2: &str, out: &str) -> Result<()> {
    let mut fin1 = BufReader::new(File::open(f1)?);
    let mut fin2 = BufReader::new(File::open(f2)?);
    let mut fout = BufWriter::new(File::create(out)?);
    let mut buf1 = String::new();
    let mut buf2 = String::new();
    loop {
        match (fin1.read_line(&mut buf1), fin2.read_line(&mut buf2)) {
            (Ok(n1), Ok(n2)) if n1 == 0 || n2 == 0 => break,
            (Ok(_), Ok(_)) => {
                writeln!(
                    fout,
                    "{}\t{}",
                    buf1.trim_end_matches('\n'),
                    buf2.trim_end_matches('\n')
                )?;
                buf1.clear();
                buf2.clear();
            }
            _ => break,
        }
    }
    Ok(())
}

/// 12. 1列目をcol1.txtに，2列目をcol2.txtに保存
/// 各行の1列目だけを抜き出したものをcol1.txtに，2列目だけを抜き出したものをcol2.txtとしてファイルに保存せよ．確認にはcutコマンドを用いよ．
fn p12() -> Result<()> {
    let mut f1 = BufWriter::new(File::create("col1.txt")?);
    let mut f2 = BufWriter::new(File::create("col2.txt")?);
    let mut buf = String::new();
    while let Ok(n) = stdin().read_line(&mut buf) {
        if n == 0 {
            break;
        }
        for (i, s) in buf.split_terminator('\t').enumerate() {
            match i {
                0 => writeln!(f1, "{}", s)?,
                1 => writeln!(f2, "{}", s)?,
                _ => break,
            };
        }
        buf.clear();
    }
    Ok(())
}

/// 11. タブをスペースに置換
/// タブ1文字につきスペース1文字に置換せよ．確認にはsedコマンド，trコマンド，もしくはexpandコマンドを用いよ．
fn p11() {
    let mut buf = String::new();
    while let Ok(n) = stdin().read_line(&mut buf) {
        if n == 0 {
            break;
        }
        let s = buf
            .as_bytes()
            .iter()
            .map(|c| if *c == '\t' as u8 { ' ' as u8 } else { *c })
            .collect::<Vec<u8>>();
        print!("{}", String::from_utf8_lossy(&s));
        buf.clear();
    }
    return;
}

/// 10. 行数のカウント
/// 行数をカウントせよ．確認にはwcコマンドを用いよ．
fn p10() {
    let mut buf = String::new();
    let mut count = 0;
    while let Ok(n) = stdin().read_line(&mut buf) {
        if n == 0 {
            break;
        }
        buf.clear();
        count += 1;
    }
    println!("{}", count);
    return;
}
