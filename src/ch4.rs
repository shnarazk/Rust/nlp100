#![allow(dead_code)]
// 第4章: 形態素解析
//
// 夏目漱石の小説『吾輩は猫である』の文章（neko.txt）をMeCabを使って形態素解析し，その結果をneko.txt.mecabというファイルに保存せよ．このファイルを用いて，以下の問に対応するプログラムを実装せよ．
//
// なお，問題37, 38, 39はmatplotlibもしくはGnuplotを用いるとよい．
use regex::Regex;
use std::collections::HashMap;
use std::fs::File;
use std::io::*;

/// # mecab format
/// 表層形\t品詞,品詞細分類1,品詞細分類2,品詞細分類3,活用型,活用形,原形,読み,発音
/// - `ある	助動詞,*,*,*,五段・ラ行アル,基本形,ある,アル,アル `
#[derive(PartialEq, Eq, Debug, Hash)]
struct Morpheme {
    surface: String,
    base: String,
    pos: String,
    pos1: String,
}

// impl Hash for Morpheme {
//     fn hash<H: Hasher>(&self, state: &mut H) {
//         self.base.hash(state);
//         self.pos.hash(state);
//         self.pos1.hash(state);
//     }
// }

fn main() {
    let mors = p30("neko.txt.mecab").expect("failed to parse");
    // let mut verbs = p31(&mors);
    // verbs.sort_unstable();
    // println!("{:?}", verbs);
    // let mut verbs2 = p32(&mors);
    // verbs2.sort_unstable();
    // println!("{:?}", verbs2);
    // let mut verbs3 = p33(&mors);
    // verbs3.sort_unstable();
    // println!("{:?}", verbs3);
    // println!("{:?}", p34(&mors));
    // println!("{:?}", p35(&mors));
    // println!("{:?}", &p36(&mors)[..50].iter().map(|m| m.base.to_string()).collect::<Vec<String>>());
    // p37(&mors);
    //p38(&mors);
    p39(&mors);
}

// 30. 形態素解析結果の読み込み
// 形態素解析結果（neko.txt.mecab）を読み込むプログラムを実装せよ．ただし，各形態素は表層形（surface），基本形（base），品詞（pos），品詞細分類1（pos1）をキーとするマッピング型に格納し，1文を形態素（マッピング型）のリストとして表現せよ．第4章の残りの問題では，ここで作ったプログラムを活用せよ．

fn p30(file: &str) -> Result<Vec<Morpheme>> {
    let mut input = BufReader::new(File::open(file)?);
    let mut vec = Vec::new();
    let mut buf = String::new();
    let valid = Regex::new(r"(?P<surface>[^\t]+)\t(?P<pos>[^,]+),(?P<pos1>[^,]+),[^,]+,[^,]+,[^,]+,[^,]+,(?P<base>[^,]+),[^,]+,[^,]+").expect("wrong regex");
    while let Ok(n) = input.read_line(&mut buf) {
        if n == 0 {
            break;
        }
        if let Some(c) = valid.captures(&buf) {
            let p = Morpheme {
                surface: c[1].to_string(),
                base: c[4].to_string(),
                pos: c[2].to_string(),
                pos1: c[3].to_string(),
            };
            vec.push(p);
        }
        buf.clear();
    }
    Ok(vec)
}

// 31. 動詞
// 動詞の表層形をすべて抽出せよ．
fn p31(ms: &[Morpheme]) -> Vec<String> {
    let mut hash: HashMap<&String, usize> = HashMap::new();
    for m in ms {
        if m.pos == "動詞" {
            hash.insert(&m.surface, 1);
        }
    }
    hash.keys().map(|k| k.to_string()).collect::<Vec<String>>()
}

// 32. 動詞の原形
// 動詞の原形をすべて抽出せよ．
fn p32(ms: &[Morpheme]) -> Vec<String> {
    let mut hash: HashMap<&String, usize> = HashMap::new();
    for m in ms {
        if m.pos == "動詞" {
            hash.insert(&m.base, 1);
        }
    }
    hash.keys().map(|k| k.to_string()).collect::<Vec<String>>()
}

// 33. サ変名詞
// サ変接続の名詞をすべて抽出せよ．
fn p33(ms: &[Morpheme]) -> Vec<String> {
    let mut hash: HashMap<&String, usize> = HashMap::new();
    for m in ms {
        if m.pos == "名詞" && m.pos1 == "サ変接続" {
            hash.insert(&m.base, 1);
        }
    }
    hash.keys().map(|k| k.to_string()).collect::<Vec<String>>()
}

// 34. 「AのB」
// 2つの名詞が「の」で連結されている名詞句を抽出せよ．
fn p34(ms: &[Morpheme]) -> Vec<String> {
    let mut hash: HashMap<String, usize> = HashMap::new();
    for i in 1..ms.len() - 2 {
        if ms[i].pos == "名詞" && ms[i + 1].surface == "の" && ms[i + 2].pos == "名詞" {
            hash.insert(format!("{}の{}", ms[i].surface, ms[i + 2].surface), 0);
        }
    }
    hash.keys().map(|k| k.to_string()).collect::<Vec<String>>()
}

// 35. 名詞の連接
// 名詞の連接（連続して出現する名詞）を最長一致で抽出せよ．
fn p35(ms: &[Morpheme]) -> Vec<String> {
    let mut hash: HashMap<String, usize> = HashMap::new();
    let mut in_sequence = false;
    let mut seq_begin: usize = 0;
    for (i, m) in ms.iter().enumerate() {
        if m.pos == "名詞" {
            if in_sequence {
                continue;
            } else {
                in_sequence = true;
                seq_begin = i;
            }
        } else if in_sequence {
            if 1 < i - seq_begin {
                let ns: String = (seq_begin..i).map(|k| ms[k].base.to_string()).collect();
                hash.insert(ns, 0);
            }
            in_sequence = false;
        }
    }
    hash.keys().map(|k| k.to_string()).collect::<Vec<String>>()
}

// 36. 単語の出現頻度
// 文章中に出現する単語とその出現頻度を求め，出現頻度の高い順に並べよ．
fn p36(ms: &[Morpheme]) -> Vec<&Morpheme> {
    let mut hash: HashMap<&Morpheme, usize> = HashMap::new();
    for m in ms {
        if let Some(n) = hash.get(m) {
            hash.insert(m, n + 1);
        } else {
            hash.insert(m, 1);
        }
    }
    let mut vec = hash
        .iter()
        .map(|(p, q)| (*q, *p))
        .collect::<Vec<(usize, &Morpheme)>>();
    vec.sort_unstable_by_key(|t| t.0);
    vec.iter().rev().map(|t| t.1).collect()
}

// 37. 頻度上位10語
// 出現頻度が高い10語とその出現頻度をグラフ（例えば棒グラフなど）で表示せよ．
fn p37(ms: &[Morpheme]) {
    let mut hash: HashMap<&Morpheme, usize> = HashMap::new();
    for m in ms {
        // if m.pos != "動詞" {
        //     continue;
        // }
        if let Some(n) = hash.get(m) {
            hash.insert(m, n + 1);
        } else {
            hash.insert(m, 1);
        }
    }
    let mut vec = hash
        .iter()
        .map(|(p, q)| (*q, *p))
        .collect::<Vec<(usize, &Morpheme)>>();
    vec.sort_unstable_by_key(|t| t.0);
    vec.reverse();
    let max = vec[0].0;
    for (n, m) in &vec[..32] {
        let len = n * 80 / max;
        println!(
            "{:<8}\t{:>5} |{}",
            m.base,
            n,
            String::from_utf8_lossy(&vec![b'*'; len])
        );
    }
}

// 38. ヒストグラム
// 単語の出現頻度のヒストグラム（横軸に出現頻度，縦軸に出現頻度をとる単語の種類数を棒グラフで表したもの）を描け．
fn p38(ms: &[Morpheme]) {
    let mut hash: HashMap<&Morpheme, usize> = HashMap::new();
    for m in ms {
        // if m.pos != "動詞" {
        //     continue;
        // }
        if let Some(n) = hash.get(m) {
            hash.insert(m, n + 1);
        } else {
            hash.insert(m, 1);
        }
    }
    let mut hash2: HashMap<usize, usize> = HashMap::new(); // (occurrences, kinds)
    for occurs in hash.values() {
        if let Some(kinds) = hash2.get(&occurs) {
            hash2.insert(*occurs, kinds + 1);
        } else {
            hash2.insert(*occurs, 1);
        }
    }
    // convert to Vec<(occurrences, kinds)>
    let mut vec = hash2
        .iter()
        .map(|(k, v)| (*k, *v))
        .collect::<Vec<(usize, usize)>>();
    vec.sort_unstable_by_key(|(occurs, _kinds)| *occurs); // sort by occurrence
    vec.reverse(); // from large to small
    let max: usize = vec.iter().map(|t| t.1).max().unwrap_or(1);
    for (occurs, kinds) in &vec {
        if *occurs < 2 {
            break;
        }
        let len = kinds * 80 / max;
        println!(
            "{:<8}, {:>8} {}",
            occurs,
            kinds,
            String::from_utf8_lossy(&vec![b'*'; len])
        );
    }
}

// 39. Zipfの法則
// 単語の出現頻度順位を横軸，その出現頻度を縦軸として，両対数グラフをプロットせよ．
fn p39(ms: &[Morpheme]) {
    let mut hash: HashMap<&Morpheme, usize> = HashMap::new();
    for m in ms {
        // if m.pos != "動詞" {
        //     continue;
        // }
        if let Some(n) = hash.get(m) {
            hash.insert(m, n + 1);
        } else {
            hash.insert(m, 1);
        }
    }
    // convert to Vec<occurs>
    let mut vec = hash
        .iter()
        .map(|(_m, occurs)| *occurs)
        .collect::<Vec<usize>>();
    vec.sort_unstable();
    vec.reverse(); // from large to small
    for (i, occurs) in vec.iter().enumerate() {
        println!(
            "{:>8.5}, {:>8.5}",
            ((i + 1) as f64).log(10.0),
            (*occurs as f64).log(10.0)
        );
    }
}
