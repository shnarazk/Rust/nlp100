use rand::seq::SliceRandom;
use std::collections::HashMap;
use std::fmt;
use std::fmt::Debug;
use std::fmt::Display;
use std::hash::Hash;

trait NGram<'a, T> {
    fn new(seq: &'a Vec<T>) -> Self;
    fn merge(&self, other: &Self) -> Self;
    fn intersect(&self, other: &Self) -> Self;
    fn diff(&self, other: &Self) -> Self;
    fn contains(&self, key: (T, T)) -> bool;
}

struct BiGram<'a, T> {
    hash: HashMap<(&'a T, &'a T), usize>,
}

impl<'a, T: Eq + Hash> NGram<'a, T> for BiGram<'a, T> {
    /// 05. n-gram
    fn new(seq: &'a Vec<T>) -> BiGram<'a, T> {
        let mut hash: HashMap<(&'a T, &'a T), usize> = HashMap::new();
        let mut old = &seq[0];
        for item in &seq[1..] {
            hash.insert((old, item), 1);
            old = &*item;
        }
        BiGram { hash }
    }
    /// 06. 集合
    fn merge(&self, other: &Self) -> Self {
        let mut hash = HashMap::new();
        for (k, v) in self.hash.iter() {
            hash.insert(*k, *v);
        }
        for (k, v) in other.hash.iter() {
            hash.insert(*k, *v);
        }
        BiGram { hash }
    }
    /// 06. 集合
    fn intersect(&self, other: &Self) -> Self {
        let mut hash = HashMap::new();
        for (k, _) in self.hash.iter() {
            if let Some(_) = other.hash.get(k) {
                hash.insert(*k, 1);
            }
        }
        BiGram { hash }
    }
    /// 06. 集合
    fn diff(&self, other: &Self) -> Self {
        let mut hash = HashMap::new();
        for (k, _) in self.hash.iter() {
            if let None = other.hash.get(k) {
                hash.insert(*k, 1);
            }
        }
        BiGram { hash }
    }
    /// 06. 集合
    fn contains(&self, key: (T, T)) -> bool {
        None != self.hash.get(&(&key.0, &key.1))
    }
}

impl<'a, T: Eq + Hash + Debug> fmt::Debug for BiGram<'a, T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "2gram: {:?}",
            self.hash
                .iter()
                .map(|((k1, k2), _)| vec![*k1, *k2])
                .collect::<Vec<Vec<&T>>>(),
        )
    }
}

fn main() {
    println!("Hello, world!");
    {
        let text = "stressed";
        println!("p00 {} => {}", &text, p00(text));
    }
    {
        let text = "パタトクカシーー";
        let pat = [1usize, 3, 5, 7];
        println!("p01 {} => {}", &text, p01(text, &pat));
        println!("p01 {} => {}", &text, p01(text, &[0, 2, 4, 6]));
    }
    {
        let t1 = "パトカー";
        let t2 = "タクシー";
        println!("p02 {} {} => {}", t1, t2, p02(t1, t2));
    }
    {
        let text = "Now I need a drink, alcoholic of course, after the heavy lectures involving quantum mechanics.";
        println!("p03 {} => {:?}", text, p03(text));
    }
    {
        let text = "Hi He Lied Because Boron Could Not Oxidize Fluorine. New Nations Might Also Sign Peace Security Clause. Arthur King Can.";
        println!("p04 {} => {:?}", text, p04(text));
    }
    {
        let text = "I am an NLPer";
        let words = text
            .split_whitespace()
            .into_iter()
            .map(|s| s.to_string())
            .collect::<Vec<String>>();
        let chars = words.iter().flat_map(|s| s.chars()).collect();
        // println!("p05 {:?} => {:?}", &words, as_bg(&p05(&words)));
        println!("p05 {:?} => {:?}", &words, BiGram::new(&words));
        println!("p05 {:?} => {:?}", &chars, BiGram::new(&chars));
    }
    {
        let t1 = "paraparaparadise".chars().map(|c| c).collect::<Vec<char>>();
        let t2 = "paragraph".chars().map(|c| c).collect::<Vec<char>>();
        let b1 = BiGram::new(&t1);
        let b2 = BiGram::new(&t2);
        println!("p06 text1: {:?}", &t1);
        println!("p06 text2: {:?}", &t2);
        println!("p06 bigram1: {:?}", &b1);
        println!("p06 bigram2: {:?}", &b2);
        println!("p06 add: {:?}", b1.merge(&b2));
        println!("p06 mul: {:?}", b1.intersect(&b2));
        println!("p06 diff: {:?}", b1.diff(&b2));
        println!("p06 1?: {:?}", b1.contains(('s', 'e')));
        println!("p06 2?: {:?}", b2.contains(('s', 'e')));
    }
    {
        println!("{}", p07(&12, &"気温", &22.4));
    }
    {
        let text = "パタトク 12 Now I need a drink, alcoholic of course, カシーー.89abc.";
        let enc = p08(text);
        println!("p08: text   {}", text);
        println!("p08: encode {}", enc);
        println!("p08: decode {}", p08(&enc));
    }
    {
        let text = "I couldn't believe that I could actually understand what I was reading : the phenomenal power of the human mind .";
        println!("p09: text {}", text);
        println!("p09: rand {}", p09(text));
    }
}

fn p00(text: &str) -> String {
    text.chars().rev().collect()
}

fn p01(text: &str, pattern: &[usize]) -> String {
    let mut result = Vec::with_capacity(pattern.len());
    let mut pi = pattern.iter();
    if let Some(mut to) = pi.next() {
        for (i, c) in text.chars().enumerate() {
            if i == *to {
                result.push(c);
                if let Some(next) = pi.next() {
                    to = next;
                } else {
                    break;
                }
            }
        }
    }
    result.iter().collect::<String>()
}

fn p02(t1: &str, t2: &str) -> String {
    let mut ti1 = t1.chars();
    let mut ti2 = t2.chars();
    let mut result = Vec::new();
    loop {
        match (ti1.next(), ti2.next()) {
            (None, None) => break,
            (Some(c), None) => result.push(c),
            (None, Some(c)) => result.push(c),
            (Some(c1), Some(c2)) => {
                result.push(c1);
                result.push(c2);
            }
        }
    }
    result.iter().collect::<String>()
}

fn p03(text: &str) -> Vec<usize> {
    text.chars()
        .filter(|c| c.is_ascii_alphanumeric() || *c == ' ')
        .collect::<String>()
        .split_whitespace()
        .map(|w| w.len())
        .collect()
}

fn p04(text: &str) -> HashMap<String, usize> {
    let mut hash = HashMap::new();
    let idx = [1, 5, 6, 7, 8, 9, 15, 16, 19];
    for (i, w) in text.split_whitespace().enumerate() {
        let key = if idx.contains(&(i + 1)) {
            w[0..2].to_string()
        } else {
            w[0..1].to_string()
        };
        hash.insert(key, i);
    }
    hash
}

fn p07<X, Y, Z>(x: &X, y: &Y, z: &Z) -> String
where
    X: Display,
    Y: Display,
    Z: Display,
{
    format!("{}時の{}は{}", x, y, z).to_string()
}

fn p08(text: &str) -> String {
    let mut l = String::new();
    for c in text.chars() {
        if 'a' <= c && c <= 'z' {
            l.push((219u8 - c as u8) as char);
        } else {
            l.push(c);
        }
    }
    l
}

fn p09(text: &str) -> String {
    let mut s = String::with_capacity(text.len());
    for (i, w) in text.split_whitespace().enumerate() {
        if 0 < i {
            s.push(' ');
        }
        if 4 <= w.len() {
            let v = w.chars().collect::<Vec<char>>();
            s.push(v[0]);
            let mut seq = (1..v.len() - 1).collect::<Vec<usize>>();
            seq.shuffle(&mut rand::thread_rng());
            for i in seq {
                s.push(v[i]);
            }
            s.push(v[v.len() - 1]);
        } else {
            s.push_str(w);
        }
    }
    s
}
