// 第5章: 係り受け解析
//
// 夏目漱石の小説『吾輩は猫である』の文章（neko.txt）をCaboChaを使って係り受け解析し，その結果をneko.txt.cabochaというファイルに保存せよ．このファイルを用いて，以下の問に対応するプログラムを実装せよ．
//
#![allow(dead_code)]
use dot;
use regex::Regex;
use std::borrow::Cow;
use std::collections::HashMap;
use std::fs::File;
use std::io::*;
use std::mem;

fn main() {
    // let v = p40("neko.txt.cabocha").expect("fail to parse");
    // println!("{:?}", v[2]);
    let v = p41("neko.txt.cabocha").expect("fail to parse");
    // for (i, c) in v[7].iter().enumerate() {
    //     println!(
    //         "{:>2} {:>2} {:?}\t{:<14}\t|{}",
    //         i,
    //         c.dst,
    //         c.srcs,
    //         c.morphs
    //             .iter()
    //             .map(|m| m.surface.to_string())
    //             .collect::<String>(),
    //         c.morphs
    //             .iter()
    //             .map(|m| m.pos.to_string())
    //             .collect::<String>(),
    //     );
    // }
    // p42(&v);
    // p43(&v);
    // p44(&v);
    // p45(&v);
    // p46(&v);
    // p47(&v);
    // p48(&v);
    p49(&v[7]);
}

//
// * 1 2D 0/1 -0.764522              // 文節番号 係り先番号 主辞/機能語の位置 スコア
// 吾輩	名詞,代名詞,一般,*,*,*,吾輩,ワガハイ,ワガハイ
// は	助詞,係助詞,*,*,*,*,は,ハ,ワ
// EOS

#[derive(PartialEq, Eq, Debug, Hash)]
struct Morph {
    surface: String,
    base: String,
    pos: String,
    pos1: String,
}

// 40. 係り受け解析結果の読み込み（形態素）
// 形態素を表すクラスMorphを実装せよ．このクラスは表層形（surface），基本形（base），品詞（pos），品詞細分類1（pos1）をメンバ変数に持つこととする．さらに，CaboChaの解析結果（neko.txt.cabocha）を読み込み，各文をMorphオブジェクトのリストとして表現し，3文目の形態素列を表示せよ．
fn p40(file: &str) -> Result<Vec<Vec<Morph>>> {
    let mut input = BufReader::new(File::open(file)?);
    let mut result = Vec::new();
    let mut buf = String::new();
    //let start = Regex::new(r"\* ([0-9]+) (-?[0-9]+)D ([0-9]+)/").expect("wrong regex");
    let tag = Regex::new(r"\* [0-9]+ -?[0-9]+D [0-9]+/[0-9]+ -?[0-9.]+").expect("wrong regex");
    let end = Regex::new(r"^EOS").expect("wrong regex");
    let word = Regex::new(r"(?P<surface>[^\t]+)\t(?P<pos>[^,]+),(?P<pos1>[^,]+),[^,]+,[^,]+,[^,]+,[^,]+,(?P<base>[^,]+),[^,]+,[^,]+").expect("wrong regex");
    let mut sentence = Vec::new();
    'next: while let Ok(n) = input.read_line(&mut buf) {
        if n == 0 {
            break;
        }
        if let Some(_) = tag.captures(&buf) {
            buf.clear();
            continue;
        }
        if let Some(_) = end.captures(&buf) {
            result.push(sentence);
            sentence = Vec::new();
            buf.clear();
            continue;
        }
        if let Some(c) = word.captures(&buf) {
            let m = Morph {
                surface: c[1].to_string(),
                base: c[4].to_string(),
                pos: c[2].to_string(),
                pos1: c[3].to_string(),
            };
            sentence.push(m);
        }
        buf.clear();
    }
    Ok(result)
}

// 41. 係り受け解析結果の読み込み（文節・係り受け）
// 40に加えて，文節を表すクラスChunkを実装せよ．このクラスは形態素（Morphオブジェクト）のリスト（morphs），係り先文節インデックス番号（dst），係り元文節インデックス番号のリスト（srcs）をメンバ変数に持つこととする．さらに，入力テキストのCaboChaの解析結果を読み込み，１文をChunkオブジェクトのリストとして表現し，8文目の文節の文字列と係り先を表示せよ．第5章の残りの問題では，ここで作ったプログラムを活用せよ．
//
#[derive(PartialEq, Eq, Debug, Hash)]
struct Chunk {
    morphs: Vec<Morph>,
    dst: isize,
    srcs: Vec<usize>,
}

impl Default for Chunk {
    fn default() -> Chunk {
        Chunk {
            morphs: Vec::new(),
            dst: 0,
            srcs: Vec::new(),
        }
    }
}

impl Chunk {
    fn contains(&self, key: &str) -> bool {
        self.morphs.iter().any(|m| m.pos == key)
    }
    fn surfaces(&self, no_sym: bool) -> String {
        self.morphs
            .iter()
            .filter(|m| m.pos != "記号" || !no_sym)
            .map(|m| m.surface.to_string())
            .collect::<String>()
    }
}

type Sentence = Vec<Chunk>;

fn p41(file: &str) -> Result<Vec<Sentence>> {
    let mut input = BufReader::new(File::open(file)?);
    let mut buf = String::new();
    let mut result: Vec<Sentence> = Vec::new();
    let mut sentence: Sentence = Vec::new();
    let mut chunk: Chunk = Chunk::default();
    let mut froms: HashMap<usize, Vec<usize>> = HashMap::new();
    let tag = Regex::new(r"\* ([0-9]+) (-?[0-9]+)D [0-9]+/[0-9]+ -?[0-9.]+").expect("wrong regex");
    let end = Regex::new(r"^EOS").expect("wrong regex");
    let word = Regex::new(r"(?P<surface>[^\t]+)\t(?P<pos>[^,]+),(?P<pos1>[^,]+),[^,]+,[^,]+,[^,]+,[^,]+,(?P<base>[^,]+),[^,]+,[^,]+").expect("wrong regex");
    'next: while let Ok(n) = input.read_line(&mut buf) {
        if n == 0 {
            break;
        }
        if let Some(c) = tag.captures(&buf) {
            let id: usize = c[1].parse().unwrap();
            let to: isize = c[2].parse().unwrap();
            if 0 < id {
                sentence.push(chunk);
                chunk = Chunk::default();
            }
            if to != -1 {
                if let Some(l) = froms.get_mut(&(to as usize)) {
                    l.push(id);
                } else {
                    froms.insert(to as usize, vec![id]);
                }
            }
            chunk.dst = to;
            buf.clear();
            continue;
        }
        if let Some(_) = end.captures(&buf) {
            sentence.push(chunk);
            for (i, c) in sentence.iter_mut().enumerate() {
                if let Some(l) = froms.get_mut(&i) {
                    mem::swap(&mut c.srcs, l);
                }
            }
            chunk = Chunk::default();
            result.push(sentence);
            sentence = Vec::new();
            buf.clear();
            froms.clear();
            continue;
        }
        if let Some(c) = word.captures(&buf) {
            let m = Morph {
                surface: c[1].to_string(),
                base: c[4].to_string(),
                pos: c[2].to_string(),
                pos1: c[3].to_string(),
            };
            chunk.morphs.push(m);
        }
        buf.clear();
    }
    Ok(result)
}

// 42. 係り元と係り先の文節の表示
// 係り元の文節と係り先の文節のテキストをタブ区切り形式ですべて抽出せよ．ただし，句読点などの記号は出力しないようにせよ．
fn p42(v: &Vec<Sentence>) {
    for s in v {
        for (j, c) in s.iter().enumerate() {
            for i in &c.srcs {
                println!("{}\t{}", s[*i].surfaces(true), s[j].surfaces(true));
            }
        }
    }
}

// 43. 名詞を含む文節が動詞を含む文節に係るものを抽出
// 名詞を含む文節が，動詞を含む文節に係るとき，これらをタブ区切り形式で抽出せよ．ただし，句読点などの記号は出力しないようにせよ．
fn p43(v: &Vec<Sentence>) {
    for s in v {
        for (j, c) in s.iter().enumerate() {
            if !s[j].contains("動詞") {
                continue;
            }
            for i in &c.srcs {
                if s[*i].contains("名詞") {
                    println!("{}\t{}", s[*i].surfaces(true), s[j].surfaces(true));
                }
            }
        }
    }
}

// 44. 係り受け木の可視化
// 与えられた文の係り受け木を有向グラフとして可視化せよ．可視化には，係り受け木をDOT言語に変換し，Graphvizを用いるとよい．また，Pythonから有向グラフを直接的に可視化するには，pydotを使うとよい．
type Nd = usize;
type Ed = (usize, usize);

struct S<'a> {
    chunks: &'a Vec<Chunk>,
}

impl<'a> dot::Labeller<'a, Nd, Ed> for S<'a> {
    fn graph_id(&'a self) -> dot::Id<'a> {
        dot::Id::new("example1").unwrap()
    }
    fn node_id(&'a self, n: &Nd) -> dot::Id<'a> {
        dot::Id::new(format!("N{}", *n)).unwrap()
    }
    /// We need to use HtmlStr; the others are going to use hex encoding for Japanese letters
    fn node_label(&'a self, n: &Nd) -> dot::LabelText<'a> {
        dot::LabelText::HtmlStr(self.chunks[*n].surfaces(true).into())
    }
}

impl<'a> dot::GraphWalk<'a, Nd, Ed> for S<'a> {
    fn nodes(&self) -> dot::Nodes<'a, Nd> {
        let v = &self.chunks;
        Cow::Owned((0..v.len()).collect())
    }

    fn edges(&'a self) -> dot::Edges<'a, Ed> {
        let v = &self.chunks;
        let mut edges: Vec<Ed> = Vec::new();
        for (j, c) in v.iter().enumerate() {
            for i in &c.srcs {
                edges.push((*i, j));
            }
        }
        Cow::Owned(edges)
    }
    fn source(&self, e: &Ed) -> Nd {
        e.0
    }
    fn target(&self, e: &Ed) -> Nd {
        e.1
    }
}

fn p44(g: &Vec<Sentence>) {
    let chunks = &g[g.len() / 2];
    let mut f = File::create("example.dot").expect("failed to create example.dot");
    // $ dot -Tpng example.dot > example.png && open example.png
    dot::render(&S { chunks }, &mut f).expect("failed to dump a dot.");
}

// 45. 動詞の格パターンの抽出
// 今回用いている文章をコーパスと見なし，日本語の述語が取りうる格を調査したい． 動詞を述語，動詞に係っている文節の助詞を格と考え，述語と格をタブ区切り形式で出力せよ． ただし，出力は以下の仕様を満たすようにせよ．
//
// 動詞を含む文節において，最左の動詞の基本形を述語とする
// 述語に係る助詞を格とする
// 述語に係る助詞（文節）が複数あるときは，すべての助詞をスペース区切りで辞書順に並べる
// 「吾輩はここで始めて人間というものを見た」という例文（neko.txt.cabochaの8文目）を考える． この文は「始める」と「見る」の２つの動詞を含み，「始める」に係る文節は「ここで」，「見る」に係る文節は「吾輩は」と「ものを」と解析された場合は，次のような出力になるはずである．
//
// 始める  で
// 見る    は を
// このプログラムの出力をファイルに保存し，以下の事項をUNIXコマンドを用いて確認せよ．
//
// コーパス中で頻出する述語と格パターンの組み合わせ
// 「する」「見る」「与える」という動詞の格パターン（コーパス中で出現頻度の高い順に並べよ）
fn p45(v: &Vec<Sentence>) {
    let mut hash: HashMap<&String, Vec<&Morph>> = HashMap::new();
    for s in v {
        for c in s {
            if let Some(verb) = c.morphs.iter().find(|c| c.pos == "動詞") {
                for i in &c.srcs {
                    for m in &s[*i].morphs {
                        if m.pos == "助詞" {
                            if let Some(l) = hash.get_mut(&verb.base) {
                                if !l.contains(&m) {
                                    l.push(m);
                                }
                            } else {
                                let v = vec![m];
                                hash.insert(&verb.base, v);
                            }
                        }
                    }
                }
            }
        }
    }
    for (verb, adj) in &hash {
        let mut ads = adj.iter().map(|m| &m.surface).collect::<Vec<&String>>();
        ads.sort_unstable();
        println!(
            "{}\t{}",
            verb,
            ads.iter()
                .fold("".to_string(), |sum, add| format!("{} {}", sum, add))
        );
    }
}

// 46. 動詞の格フレーム情報の抽出
// 45のプログラムを改変し，述語と格パターンに続けて項（述語に係っている文節そのもの）をタブ区切り形式で出力せよ．45の仕様に加えて，以下の仕様を満たすようにせよ．
//
// 項は述語に係っている文節の単語列とする（末尾の助詞を取り除く必要はない）
// 述語に係る文節が複数あるときは，助詞と同一の基準・順序でスペース区切りで並べる
// 「吾輩はここで始めて人間というものを見た」という例文（neko.txt.cabochaの8文目）を考える． この文は「始める」と「見る」の２つの動詞を含み，「始める」に係る文節は「ここで」，「見る」に係る文節は「吾輩は」と「ものを」と解析された場合は，次のような出力になるはずである．
//
// 始める  で      ここで
// 見る    は を   吾輩は ものを
fn p46(v: &Vec<Sentence>) {
    let mut hash: HashMap<&String, Vec<(&Morph, String)>> = HashMap::new();
    for s in v {
        for c in s {
            if let Some(verb) = c.morphs.iter().find(|c| c.pos == "動詞") {
                for i in &c.srcs {
                    for m in &s[*i].morphs {
                        if m.pos == "助詞" {
                            if let Some(l) = hash.get_mut(&verb.base) {
                                if let None = l.iter().find(|(a, _)| a == &m) {
                                    l.push((m, s[*i].surfaces(true)));
                                }
                            } else {
                                let v = vec![(m, s[*i].surfaces(true))];
                                hash.insert(&verb.base, v);
                            }
                        }
                    }
                }
            }
        }
    }
    for (verb, adj) in &hash {
        let mut a = adj.clone();
        a.sort_unstable_by_key(|t| &t.0.surface);
        println!(
            "{}\t{}\t{}",
            verb,
            a.iter().fold("".to_string(), |sum, add| format!(
                "{} {}",
                sum, add.0.surface
            )),
            a.iter()
                .fold("".to_string(), |sum, add| format!("{} {}", sum, add.1)),
        );
    }
}

// 47. 機能動詞構文のマイニング
// 動詞のヲ格にサ変接続名詞が入っている場合のみに着目したい．46のプログラムを以下の仕様を満たすように改変せよ．
//
// 「サ変接続名詞+を（助詞）」で構成される文節が動詞に係る場合のみを対象とする
// 述語は「サ変接続名詞+を+動詞の基本形」とし，文節中に複数の動詞があるときは，最左の動詞を用いる
// 述語に係る助詞（文節）が複数あるときは，すべての助詞をスペース区切りで辞書順に並べる
// 述語に係る文節が複数ある場合は，すべての項をスペース区切りで並べる（助詞の並び順と揃えよ）
// 例えば「別段くるにも及ばんさと、主人は手紙に返事をする。」という文から，以下の出力が得られるはずである．
//
// 返事をする      と に は        及ばんさと 手紙に 主人は
// このプログラムの出力をファイルに保存し，以下の事項をUNIXコマンドを用いて確認せよ．
//
// コーパス中で頻出する述語（サ変接続名詞+を+動詞）
// コーパス中で頻出する述語と助詞パターン
fn p47(v: &Vec<Sentence>) {
    let mut hash: HashMap<String, HashMap<&String, Vec<String>>> = HashMap::new();
    for s in v {
        for c in s {
            if let Some(verb) = c.morphs.iter().find(|m| m.pos == "動詞") {
                if let Some(a) = c.srcs.iter().find(|ci| {
                    1 < s[**ci].morphs.len()
                        && (0..s[**ci].morphs.len() - 1).any(|i| {
                            s[**ci].morphs[i].pos1 == "サ変接続"
                                && s[**ci].morphs[i + 1].base == "を"
                        })
                }) {
                    let key = format!("{}{}", s[*a].surfaces(true), verb.base);
                    for i in &c.srcs {
                        if i == a {
                            continue;
                        }
                        for m in &s[*i].morphs {
                            if m.pos == "助詞" {
                                if let Some(hs) = hash.get_mut(&key) {
                                    let seq = s[*i].surfaces(true);
                                    if let Some(l) = hs.get_mut(&m.base) {
                                        if !l.contains(&seq) {
                                            l.push(seq);
                                        }
                                    } else {
                                        hs.insert(&m.base, vec![seq].to_vec());
                                    }
                                } else {
                                    let mut h = HashMap::new();
                                    h.insert(&m.base, vec![s[*i].surfaces(true)]);
                                    hash.insert(key.to_string(), h);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    for (key, adj) in &hash {
        let mut a = adj
            .iter()
            .map(|(k, v)| (*k, v[..].join(" ")))
            .collect::<Vec<(&String, String)>>();
        a.sort_unstable_by_key(|t| t.0);
        println!(
            "{}\t{}\t{}",
            key,
            a.iter()
                .fold("".to_string(), |sum, add| format!("{} {}", sum, add.0)),
            a.iter()
                .fold("".to_string(), |sum, add| format!("{} {}", sum, add.1)),
        );
    }
}

// 48. 名詞から根へのパスの抽出
// 文中のすべての名詞を含む文節に対し，その文節から構文木の根に至るパスを抽出せよ． ただし，構文木上のパスは以下の仕様を満たすものとする．
//
// 各文節は（表層形の）形態素列で表現する
// パスの開始文節から終了文節に至るまで，各文節の表現を"->"で連結する
// 「吾輩はここで始めて人間というものを見た」という文（neko.txt.cabochaの8文目）から，次のような出力が得られるはずである．
//
// 吾輩は -> 見た
// ここで -> 始めて -> 人間という -> ものを -> 見た
// 人間という -> ものを -> 見た
// ものを -> 見た
fn p48(v: &Vec<Sentence>) {
    for s in v {
        for c in s {
            if let Some(_) = c.morphs.iter().find(|m| m.pos == "名詞") {
                print!("{}", c.surfaces(true));
                let mut target: isize = c.dst;
                while target != -1 {
                    let c = &s[target as usize];
                    print!(" -> {}", c.surfaces(true));
                    target = c.dst;
                }
                println!();
            }
        }
    }
}

// 49. 名詞間の係り受けパスの抽出
// 文中のすべての名詞句のペアを結ぶ最短係り受けパスを抽出せよ．ただし，名詞句ペアの文節番号がi
// とj （i<j）のとき，係り受けパスは以下の仕様を満たすものとする．
//
// 問題48と同様に，パスは開始文節から終了文節に至るまでの各文節の表現（表層形の形態素列）を"->"で連結して表現する
// 文節iとjに含まれる名詞句はそれぞれ，XとYに置換する
// また，係り受けパスの形状は，以下の2通りが考えられる．
//
// 文節iから構文木の根に至る経路上に文節jが存在する場合:
//   文節iから文節jのパスを表示
// 上記以外で，文節iと文節jから構文木の根に至る経路上で共通の文節kで交わる場合:
//   - 文節iから文節kに至る直前のパスと
//   - 文節jから文節kに至る直前までのパス，
//   - 文節kの内容
// を"|"で連結して表示
// 例えば，「吾輩はここで始めて人間というものを見た。」という文（neko.txt.cabochaの8文目）から，次のような出力が得られるはずである．
//
// Xは | Yで -> 始めて -> 人間という -> ものを | 見た
// Xは | Yという -> ものを | 見た
// Xは | Yを | 見た
// Xで -> 始めて -> Y
// Xで -> 始めて -> 人間という -> Y
// Xという -> Y

impl Chunk {
    fn abst_surfaces(&self, label: &str) -> String {
        self.morphs
            .iter()
            .map(|m| {
                if m.pos == "名詞" {
                    label.to_string()
                } else {
                    m.surface.to_string()
                }
            })
            .collect::<String>()
    }
}

trait Chunks {
    fn path_on(&self, target: usize) -> Vec<usize>;
}

impl Chunks for Vec<Chunk> {
    fn path_on(&self, start: usize) -> Vec<usize> {
        let mut vec: Vec<usize> = Vec::new();
        let mut target: isize = start as isize;
        while target != -1 {
            vec.push(target as usize);
            target = self[target as usize].dst;
        }
        vec
    }
}

fn p49(s: &Sentence) {
    let map_surfaces = |v: Vec<usize>, i, j| {
        v.iter()
            .map(|k| {
                if *k == i {
                    s[*k].abst_surfaces("X")
                } else if *k == j {
                    s[*k].abst_surfaces("Y")
                } else {
                    s[*k].surfaces(true)
                }
            })
            .collect::<Vec<String>>()
            .join(" -> ")
    };
    for (i, c) in s.iter().enumerate() {
        if c.contains("名詞") {
            let path = s.path_on(i);
            for (j, d) in s.iter().enumerate().skip(i + 1) {
                if d.contains("名詞") {
                    // println!("# {} {}", i, d.surfaces(true));
                    match merge_pathes(&path, &s.path_on(j)) {
                        (p, None) => println!("{} -> Y", map_surfaces(p, i, j)),
                        (p1, Some((p2, p3))) => {
                            println!(
                                "{} | {} | {}",
                                map_surfaces(p1, i, j),
                                map_surfaces(p2, i, j),
                                map_surfaces(p3, i, j)
                            );
                        }
                    }
                }
            }
        }
    }
}

fn merge_pathes(p1: &[usize], p2: &[usize]) -> (Vec<usize>, Option<(Vec<usize>, Vec<usize>)>) {
    if let Some(x) = p1.iter().position(|i| i == &p2[0]) {
        return (p1[..x].to_vec(), None);
    }
    let p1_merge: usize;
    let mut p2_merge: usize = 1;
    loop {
        if let Some(x) = p1.iter().position(|i| i == &p2[p2_merge]) {
            p1_merge = x;
            break;
        }
        p2_merge += 1;
    }
    return (
        p1[..p1_merge].to_vec(),
        Some((p2[..p2_merge].to_vec(), p1[p1_merge..].to_vec())),
    );
}
