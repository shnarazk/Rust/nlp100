# nlp100

http://www.cl.ecei.tohoku.ac.jp/nlp100/

### useful crates

- regex
- serde
- serde_json
- serde-xml-rs
- combine



## Start Databases

### redis

- install: `nix-env -i redis`
- start: `redis-server`
- shutdown: `redis-cli shutdown`

### MongoDB

- install: `nix-env -i mongodb`
- prepeare: `mkdir mongo`
- start: `mongod --unixSocketPrefix=mongo --dbpath=mongo --port=10060 --logpath=mongo/mongod.log --pidfilepath=mongo/mongod.pid --noprealloc --nojournal`
- shutdown: ?


### 2019-04-20

第7章68のテストラン：

```
tag: industrial
  1	  25	"Rammstein"
  2	  25	"Nine Inch Nails"
  3	  23	"The Prodigy"
  4	  15	"Kraftwerk"
  5	  10	"Einstürzende Neubauten"
  6	   9	"KMFDM"
  7	   6	"Ministry"
  8	   5	"Swans"
  9	   5	"Front 242"
 10	   4	"Vladimír Hirsch"
 11	   4	"Gravity Kills"
 12	   4	"Gary Numan"
 13	   4	"Cabaret Voltaire"
 14	   4	"Apoptygma Berzerk"
 15	   4	":wumpscut:"
 16	   3	"Mad Essence"
 17	   3	"Hocico"
 18	   3	"Front Line Assembly"
 19	   3	"Emilie Autumn"
 20	   3	"Atari Teenage Riot"
```

なぜSkinny Puppyが出てこない？ 全然納得いかん。バグってる！
