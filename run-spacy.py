import spacy

f = open("nlp.txt")
nlp = spacy.load('en_core_web_sm')
doc = nlp(f.read())  # nlp(u'Apple is looking at buying U.K. startup for $1 billion')

# for token in doc:
# #     print(token.text, token.lemma_, token.pos_, token.tag_, token.dep_,
# #           token.shape_, token.is_alpha, token.is_stop)
#     print(token.text, "\t", token.lemma_, token.pos_, token.tag_)

print(doc.print_tree())
